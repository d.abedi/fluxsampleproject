import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import "./../actions"
import "./../stores"

Rectangle{
    id: markerRoot

    property bool isPlay: MainStore.appSetting.isPlay
    onIsPlayChanged: {
        if(isPlay){
            anim.start()
        }
        else{
            anim.stop()
            markerPointer.x = 2
        }
    }

    width: 100
    height: 15

    RowLayout{
        anchors.fill: parent

        //-- tools section place --//
        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 100
        }

        //-- wave section --//
        Rectangle{
            id: waveRec

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.leftMargin: 5

            color: "#00FF0000"
            border.width: 1
            border.color: "#b8b8b8"

            Rectangle{
                id: markerPointer

                width: 6
                height: parent.height - 4
                y: 2
                x: 2

                radius: 4
                color: "#989898"

            }
        }

        //-- gain section place --//
        Item{
            Layout.fillHeight: true
            Layout.preferredWidth: 16
        }

    }


    NumberAnimation {
        id: anim

        target: markerPointer
        property: "x"
        duration: 1000 * 20
        //easing.type: Easing.InOutQuad
        from: 2
        to: waveRec.width - 8

        onStopped: {
            AppActions.stop()
        }
    }
}
