import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../actions"

ItemDelegate{

    property alias title: lbl_title.text

    property bool isActivated: false

    Layout.preferredHeight: lbl_title.implicitHeight + 4
    Layout.preferredWidth: height

    
    Label{
        id: lbl_title
        text: "F1"
        anchors.centerIn: parent
        font.pixelSize: Qt.application.font.pixelSize * 0.7
        font.bold: true
        color: isActivated ? "blue" : "grey"
        renderType: Text.NativeRendering
    }

    //-- hovere overlay --//
    Rectangle{
        anchors.fill: parent
        color: "#00000000"
        border.width: 1
        border.color: "#b4b4b4"
    }

    onClicked: {
        isActivated = !isActivated
    }

}
