import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../actions"

ItemDelegate{

    property alias source: img.source

    property bool isActivated: false

    Layout.preferredHeight: lbl_sizePorpose.implicitHeight + 4
    Layout.preferredWidth: height

    
    //-- size porpose --//
    Label{
        id: lbl_sizePorpose
        visible: false
        text: "S"
        font.pixelSize: Qt.application.font.pixelSize * 0.7
        font.bold: true
    }

    //-- hovere overlay --//
    Rectangle{
        anchors.fill: parent
        color: "#00000000"

        Image {
            id: img
            source: "qrc:/views/Tools/Images/trush.png"
            sourceSize.width: parent.width
            sourceSize.height: parent.height
        }
    }

    onClicked: {
        isActivated = !isActivated
    }

}
