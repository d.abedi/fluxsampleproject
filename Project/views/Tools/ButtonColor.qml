import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../actions"

ItemDelegate{

    property alias trackColor: itmColor.color


    Layout.preferredHeight: lbl_sizePorpose.implicitHeight + 4
    Layout.preferredWidth: height

    //-- size porpose --//
    Label{
        id: lbl_sizePorpose
        visible: false
        text: "S"
        font.pixelSize: Qt.application.font.pixelSize * 0.7
        font.bold: true
    }

    //-- color --//
    Rectangle{
        id: itmColor
        anchors.fill: parent
        color: "#ff0000"
        border.width: 1
        border.color: "#b4b4b4"
    }

    onClicked: {
    }

}
