import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../actions"

Item {
    height: btn_add.implicitHeight + 10

    //--border --//
    Rectangle{
        anchors.fill: parent
        color: "#00FF0000"
        border.width: 1
        border.color: "#e5e5e5"
    }

    RowLayout{
        anchors.fill: parent
        anchors.margins: 5

        //-- play/puase button --//
        Button{
            id: btn_add

            text: "Add Track"
            onClicked: {
                AppActions.addTrack("sample", "address file")
            }
        }

        //-- spacer --//
        Item{Layout.fillWidth: true}

    }
}

