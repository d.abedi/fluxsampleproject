import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QuickFlux 1.0
import AmplitudePainter 1.0
import "../stores"

Item{
    clip: true

    ColumnLayout{
        anchors.fill: parent


        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            model:  MainStore.track.model

            delegate: TrackItem{

                id:item
                uid: model.uid
                title: model.title
                file: model.file
                gain: model.gain
                filter1: model.filter1
                filter2: model.filter2
                isVisible: model.visible
                trackColor: model.color
                isPlay: MainStore.appSetting.isPlay


                //checked: model.done

            }

        }

        RangeSlider{
            id: slider
            Layout.fillWidth: true
            Layout.preferredHeight: 30
            from: 0.0
            to: 1.0
            first.value: 0.0
            second.value: 1.0
            stepSize: 0.01
            first.onMoved: {
                painterAmplitude.plotStartPrc = first.value
                console.log("first.value changed to " + first.value)
            }
            second.onMoved:{
                painterAmplitude.plotEndPrc = second.value
                console.log("first.value changed to " + (second.value+0.01))
            }


        }

        Item{
            Layout.fillWidth: true
            Layout.preferredHeight: 200

            AmplitudePainter{
                id: painterAmplitude
                anchors.fill: parent

                objectName: "AmplitudePainterMainWindow"

                plotWidth: width
                plotHeight: height
//                isDarkTheme: MainStore.appSetting.isThemeDark
                plotColor: "#ff0000"

            }

        }
    }

}
