import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../../stores"
import "../../actions"

ApplicationWindow {
    id: filter1

    width: 500
    height: 200
    title: "تنظیمات فیلتر دوم"

    Pane{
        width: parent.width
        height: parent.height

        Rectangle{
            anchors.fill: parent
            border.width: 1
            border.color: "#e4e4e4"
        }

        ColumnLayout{
            anchors.fill: parent
            anchors.margins: 10

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 50

                RowLayout{
                    anchors.fill: parent

                    Label{
                        text: "Coeff. 1:"
                    }

                    TextField{
                        id: txf_coeff1
                        placeholderText: "ضریب نخست"
                        text: MainStore.filter2.coeff_1
                    }
                }


            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 50

                RowLayout{
                    anchors.fill: parent

                    Label{
                        text: "Coeff. 2:"
                    }

                    TextField{
                        id: txf_coeff2
                        placeholderText: "ضریب دوم"
                        text: MainStore.filter2.coeff_2
                    }
                }


            }

            Button{
                text: "Confirm"
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    AppActions.setFilter2(parseFloat(txf_coeff1.text), parseFloat(txf_coeff2.text))
                }
            }

            Item {Layout.fillHeight: true}
        }
    }
}
