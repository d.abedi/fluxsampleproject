import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../actions"
import "../stores"
import "Filters"

Item {
    height: btn_play.implicitHeight + 10

    Rectangle{
        anchors.fill: parent
        color: "#00FF0000"
        border.width: 1
        border.color: "#e5e5e5"
    }

    RowLayout{
        anchors.fill: parent
        anchors.margins: 5

        //-- play/puase button --//
        Button{
            id: btn_play

            text: MainStore.appSetting.isPlay ? "stop" : "play"
            onClicked: {

                if(MainStore.appSetting.isPlay){
                    AppActions.stop()
                }
                else{
                    AppActions.play()
                }
            }
        }

        //-- spacer --//
        Item{Layout.fillWidth: true}

        //-- filter1 Setting button --//
        Button{
            id: btn_filter1Setting

            text: "filter1 Setting"
            onClicked: {
                AppActions.openFilter1Setting()
            }
        }

        //-- filter2 Setting button --//
        Button{
            id: btn_filter2Setting

            text: "filter2 Setting"
            onClicked: {
                AppActions.openFilter2Setting()
            }
        }
    }

}

