import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import "../actions"
import "Tools"

Rectangle {
    id: item
    color: "white"
    width: parent.width
    height: 60

    property int uid;
    property string title
    property string file: ""
    property int gain; //: sliderGain.value;
    property alias filter1: btn_f1.isActivated
    property alias filter2: btn_f2.isActivated
    property alias isVisible: btn_visible.isActivated
    property alias trackColor: btn_color.trackColor
    property bool isPlay: false

    onIsPlayChanged: {
        if(isPlay){

            if(!isVisible) return

            if(playMusic.source !== "") playMusic.play()
        }
        else{
            if(playMusic.source !== "") playMusic.stop()
        }
    }

    function setTrackProperties(){
        //print(uid, filter1, filter2, isVisible)
        AppActions.setTrack(uid, filter1, filter2, isVisible)
    }

    onFilter1Changed: setTrackProperties();
    onFilter2Changed: setTrackProperties();
    onIsVisibleChanged: setTrackProperties();


    RowLayout{
        anchors.fill: parent
        anchors.margins: 5

        //-- tools section --//
        Rectangle{
            id: toolsSection

            Layout.fillHeight: true
            Layout.preferredWidth: 100

            color: "#00FF0000"
            border.width: 1
            border.color: "#a2a2a2"

            ColumnLayout{
                anchors.fill: parent

                //-- title --//
                Item {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label{
//                        anchors.centerIn: parent
                        width: parent.width
                        horizontalAlignment: Qt.AlignHCenter
                        verticalAlignment: Qt.AlignVCenter
                        elide: Text.ElideRight
                        text: uid + " - " + title
                    }
                }

                //-- tools button --//
                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 20

                    RowLayout{
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        anchors.rightMargin: 5
                        anchors.bottomMargin: 5
                        spacing: 1

                        //-- filter 1 button --//
                        ButtonTool {
                            id: btn_f1

                            title: "F1"
                        }

                        //-- filter 2 button --//
                        ButtonTool {
                            id: btn_f2

                            title: "F2"
                        }

                        //-- color button --//
                        ButtonColor {
                            id: btn_color

                            trackColor: "red"
                            onClicked: {
                                AppActions.setColor(uid, trackColor)
                            }
                        }

                        Item{Layout.fillWidth: true}

                        //-- visible --//
                        ButtonIcon {
                            id: btn_visible

                            source: isActivated ? "qrc:/views/Tools/Images/eye.png" : "qrc:/views/Tools/Images/eye_close.png"
                        }

                        //-- trush --//
                        ButtonIcon {
                            id: btn_delete

                            source: "qrc:/views/Tools/Images/trush.png"

                            onClicked: {

                                AppActions.deleteTrack(uid)
                            }
                        }
                    }
                }
            }
        }

        //-- wave section --//
        Rectangle{
            id: waveSection

            Layout.fillHeight: true
            Layout.fillWidth: true

            color: "#00FF0000"
            border.width: 1
            border.color: "#a2a2a2"

            Rectangle{
                anchors.fill: parent
                anchors.margins: 5
                opacity: 0.3
                color: trackColor
                clip: true

                Label{
                    anchors.centerIn: parent
                    text: title
                }
            }
        }

        //-- gain section --//
        Rectangle{
            id: gainSection

            Layout.fillHeight: true
            Layout.preferredWidth: sliderGain.implicitWidth

            color: "#00FF0000"

            Slider{
                id: sliderGain

                from: 1
                value: gain
                to: 100

                height: parent.height
                orientation: Qt.Vertical

                handle: Rectangle {
                    x: sliderGain.leftPadding + sliderGain.visualPosition * (sliderGain.availableWidth - width)
                    y: sliderGain.topPadding + sliderGain.availableHeight / 2 - height / 2
                    implicitWidth: 0
                    implicitHeight: 0
                    radius: 2
                    color: sliderGain.pressed ? "#f0f0f0" : "#f6f6f6"
                    border.color: "#bdbebf"
                }

                onValueChanged: {

                    var num = Math.floor(sliderGain.value)

                    //-- ignore same value --//
                    if(gain === num) return;

                    AppActions.setGain(uid, num);
                }

                Label{
                    text: sliderGain.value.toFixed(0)
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -5

                }
            }

        }

    }

    //-- player --//
    MediaPlayer {
        id: playMusic
        source: file
    }
}
