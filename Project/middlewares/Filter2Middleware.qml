import QtQuick 2.0
import QuickFlux 1.1
import QtQuick.Dialogs 1.2
import "../actions"
import "../stores"
import "../views/Filters"

Middleware {

    property RootStore store: MainStore

    Filter2{
        id: filter2Win
    }

    //property var mainWindow: null

    function dispatch(type, message) {
        if (type === ActionTypes.setTrack) {

            //-- check filter2 is activated --//
            if(message.filter2 === true){

                // TODO: C class for gain operation
                //-- Gain operation on track --//
                print('|')
                print(`filter2 operation on ${message.uid} track in Middleware`)
            }

        }
        else if (type === ActionTypes.setFilter2) {

            //-- Gain operation on track --//
            print('|')
            print(`Coeff. of filter2 change to ${message.coeff1} and ${message.coeff2} track in Middleware`)
            filter2Win.visible = false

        }
        else if (type === ActionTypes.openFilter2Setting) {

            filter2Win.visible = true
            return;
        }
        next(type, message);
    }

    /*Connections {
        target: mainWindow
        onClosing: {
            // You may inject a hook to forbid closing or save data if necessary
            console.log("closing");
        }
    }*/

}

