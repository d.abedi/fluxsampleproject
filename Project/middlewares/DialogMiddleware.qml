import QtQuick 2.0
import QuickFlux 1.1
import QtQuick.Dialogs 1.2
import "../actions"
import "../stores"

Middleware {

    property RootStore store: MainStore

    MessageDialog {
        id: dialog

        property var msg

        title: "تاییدیه"
        text: "آیا شما از حذف این آیتم مطمئن هستید؟"
        standardButtons: StandardButton.Ok | StandardButton.Cancel

        onAccepted: {

            next(ActionTypes.deleteTrack, msg);
        }

        onRejected: {
            /// Trigger the changed signal even it is unchanged. It forces the checkbox to be turned off.
            //store.userPrefs.showCompletedTasksChanged();
        }
    }

    function dispatch(type, message) {

        if (type === ActionTypes.deleteTrack) {
            // If user want to show completed tasks, drop the action and show a dialog
            dialog.msg = message
            dialog.open();
            return;
        }

        /// Pass the action to next middleware / store
        next(type, message);
    }

}
