import QtQuick 2.0
import QuickFlux 1.1
import QtQuick.Dialogs 1.2
import "../actions"
import "../stores"

Middleware {

    property RootStore store: MainStore

    function dispatch(type, message) {
        if (type === ActionTypes.setGain) {

            // TODO: C class for gain operation
            //-- Gain operation on track --//
            print('|')
            print(`Gain ${message.gain} operation on track ${message.uid} in Middleware`)

            //return;
        }
        next(type, message);
    }
}

