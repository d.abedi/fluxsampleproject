import QtQuick 2.0
import QuickFlux 1.1
//import QtQuick.Dialogs 1.2
import Qt.labs.platform 1.1
import "../actions"
import "../stores"

Middleware {

    property RootStore store: MainStore

    FileDialog {
        id: dialog

        //currentFile: document.source
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)

        property var msg

        onRejected: {
        }

        onAccepted: {
            //print(dialog.file)
            msg.file= dialog.file.toString()
            var t = msg.file.split("/")
            msg.title= t[t.length-1].split(".")[0]
            //print(JSON.stringify(msg))
            next(ActionTypes.addTrack, msg);
        }

    }


    function dispatch(type, message) {

        //print("---- ", type)

        if (type === ActionTypes.addTrack) {
            print("add task pipline")
            // If user want to show completed tasks, drop the action and show a dialog
            dialog.msg = message
            dialog.open();
            return;
        }

        /// Pass the action to next middleware / store
        next(type, message);
    }

}
