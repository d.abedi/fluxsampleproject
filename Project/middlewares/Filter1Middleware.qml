import QtQuick 2.0
import QuickFlux 1.1
import QtQuick.Dialogs 1.2
import "../actions"
import "../stores"
import "../views/Filters"

Middleware {

    property RootStore store: MainStore

    Filter1{
        id: filter1Win
    }

    //property var mainWindow: null

    function dispatch(type, message) {
        if (type === ActionTypes.setTrack) {

            //-- check filter1 is activated --//
            if(message.filter1 === true){

                // TODO: C class for gain operation
                //-- Gain operation on track --//
                print('|')
                print(`filter1 operation on ${message.uid} track in Middleware`)
            }

            //return;
        }
        else if (type === ActionTypes.setFilter1) {

            //-- Gain operation on track --//
            print('|')
            print(`Coeff. of filter1 change to ${message.coeff1} and ${message.coeff2} track in Middleware`)
            filter1Win.visible = false

            //return;
        }
        else if (type === ActionTypes.openFilter1Setting) {

            filter1Win.visible = true
            return;
        }

        next(type, message);
    }

    /*Connections {
        target: mainWindow
        onClosing: {
            // You may inject a hook to forbid closing or save data if necessary
            console.log("closing");
        }
    }*/

}

