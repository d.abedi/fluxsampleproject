import QtQuick 2.0
import QuickFlux 1.1

Store {

    // It hold the todo list data model
    property alias track: track

    TrackStore {
        id: track
    }

    property alias appSetting: appSetting

    AppSettingStore {
        id: appSetting
    }

    property alias filter1: filter1

    Filter1Store {
        id: filter1
    }

    property alias filter2: filter2

    Filter2Store {
        id: filter2
    }

}
