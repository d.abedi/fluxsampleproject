/** Todo Item Store

  A centralized data store of Todo list.

 */
import QtQuick 2.0
import QuickFlux 1.1
import "../actions"

Store {

    property bool isPlay: false

    Filter {
        type: ActionTypes.play
        onDispatched: {
            isPlay = true
        }
    }

    Filter {
        type: ActionTypes.stop
        onDispatched: {
            isPlay = false
        }
    }
}



