/** Todo Item Store

  A centralized data store of Todo list.

 */
import QtQuick 2.0
import QuickFlux 1.1
import "../actions"

Store {
    property alias model: model

    property int nextUid: 4;

    ListModel {
        // Initial data
        id: model

        ListElement {
            uid: 0
            title: "Track Zero"
            file: ""
            gain: 0
            filter1: false
            filter2: false
            visible: true
            color: "red"
        }

        ListElement {
            uid: 1
            title: "Track A"
            file: ""
            gain: 0
            filter1: false
            filter2: false
            visible: true
            color: "green"
        }

        ListElement {
            uid: 2
            title: "Track B"
            file: ""
            gain: 50
            filter1: false
            filter2: false
            visible: true
            color: "blue"
        }

        ListElement {
            uid: 3
            title: "Track C"
            file: ""
            gain: 0
            filter1: false
            filter2: false
            visible: true
            color: "yellow"
        }
    }

    Filter {
        // Filter - Add a filter rule to parent

        // Filter item listens on parent's dispatched signal,
        // if a dispatched signal match with its type, it will
        // emit its own "dispatched" signal. Otherwise, it will
        // simply ignore the signal.

        type: ActionTypes.addTrack
        onDispatched: {
            var item = {
                uid: nextUid++,
                title: message.title,
                file: message.file,
                gain: 0,
                filter1: false,
                filter2: false,
                visible: true,
                color: "yellow"
            }
            model.append(item);
        }
    }


    //-- set Track properties filter --//
    Filter {
        type: ActionTypes.setTrack
        onDispatched: {
            for (var i = 0 ; i < model.count ; i++) {
                var item  = model.get(i);
                if (item.uid === message.uid) {

                    //-- check old value --//
                    if(model.get(i).filter1 !== message.filter1) model.setProperty(i,"filter1",message.filter1);
                    if(model.get(i).filter2 !== message.filter2) model.setProperty(i,"filter2",message.filter2);
                    if(model.get(i).visible !== message.visible) model.setProperty(i,"visible",message.visible);
                    //if(model.get(i).color !== message.color) model.setProperty(i,"color",message.color);

                    //print(JSON.stringify(model.get(i)))

                    break;
                }
            }
        }
    }

    //-- set Gain filter --//
    Filter {
        type: ActionTypes.setGain
        onDispatched: {
            for (var i = 0 ; i < model.count ; i++) {
                var item  = model.get(i);
                if (item.uid === message.uid) {

                    model.setProperty(i,"gain",message.gain);
                    print(`set ${message.gain} into ${i}`)
                    break;
                }
            }
        }
    }

    //-- set Color filter --//
    Filter {
        type: ActionTypes.setColor
        onDispatched: {
            for (var i = 0 ; i < model.count ; i++) {
                var item  = model.get(i);
                if (item.uid === message.uid) {

                    model.setProperty(i,"color",message.color);
                    print(`set color ${message.color} into ${i}`)
                    break;
                }
            }
        }
    }

    //-- delete filter --//
    Filter {
        type: ActionTypes.deleteTrack
        onDispatched: {
            for (var i = 0 ; i < model.count ; i++) {
                var item  = model.get(i);
                if (item.uid === message.uid) {

                    model.remove(i)
                    print(`remove Item ${message.uid}`)
                    break;
                }
            }
        }
    }

}



