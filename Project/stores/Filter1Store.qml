/** Todo Item Store

  A centralized data store of Todo list.

 */
import QtQuick 2.0
import QuickFlux 1.1
import "../actions"

Store {

    property real coeff_1: 0.1
    property real coeff_2: 0.2

    Filter {
        type: ActionTypes.setFilter1
        onDispatched: {

            coeff_1 = message.coeff1
            coeff_2 = message.coeff2
        }
    }

}



