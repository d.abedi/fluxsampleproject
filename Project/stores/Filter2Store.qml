/** Todo Item Store

  A centralized data store of Todo list.

 */
import QtQuick 2.0
import QuickFlux 1.1
import "../actions"

Store {
    property real coeff_1: 0.9
    property real coeff_2: 0.5

    Filter {
        type: ActionTypes.setFilter2
        onDispatched: {

            coeff_1 = message.coeff1
            coeff_2 = message.coeff2
        }
    }
}



