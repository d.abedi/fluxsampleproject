#include "amplitudepainter.h"
#include <algorithm>
#include <QRandomGenerator>
#include "qnanopainter.h"
//#include <sec_api/stralign_s.h>


AmplitudePainter::AmplitudePainter()
{
}

void AmplitudePainter::synchronize(QNanoQuickItem *item)
{

    // Setting values here synchronized
    AmplitudeItem *realItem = static_cast<AmplitudeItem*>(item);
    if (realItem) {

        m_plotWidth = realItem->m_plotWidth;
        m_plotHeight = realItem->m_plotHeight;
        m_plotColor = realItem->m_plotColor;
        m_plotStartPrc = realItem->m_plotStartPrc;
        m_plotEndPrc = realItem->m_plotEndPrc;
        data2 = realItem->data2;
        m_max = realItem->m_max;
        m_min = realItem->m_min;

        qDebug() << "synchronize " << m_plotWidth << data2.size();
    }
}

void AmplitudePainter::paint(QNanoPainter *painter)
{
    int leftMargin = 2;
    int rightMargin = 25;
    int topMargin = 5;
    int bottomMargin = 5;
    int stepSize = 25;
    int dashSize = 3;

    try {

        //-- grid and captions --//
        DrawGridCaption(painter, leftMargin, rightMargin, topMargin, bottomMargin, stepSize, dashSize, 0);

        if(data2.size() == 0){
            qDebug() << "data is empty.";
            return;
        }

        painter->setAntialias(0.90);

        //-- draw signal --//
//        painter->setLineCap(QNanoPainter::CAP_ROUND);
        painter->setLineCap(QNanoPainter::CAP_SQUARE);
        painter->setLineJoin(QNanoPainter::JOIN_ROUND);
        painter->setLineWidth(1); //lineWidth


        QNanoColor lineColor(String2QnanoColor(m_plotColor));
        //    lineColor.setAlpha(255);
        painter->setStrokeStyle(lineColor);
        painter->setAntialias(1);

        //-- plot heigh adjustment --//
        long long diff = (m_max - m_min);
        double ratio = (double)(m_plotHeight-topMargin-bottomMargin) / diff;

        //-- width adjustment --//
        int startIndex = (data2.size()-1) * m_plotStartPrc;
        int endIndex = (data2.size()-1) * m_plotEndPrc;

        if(startIndex < 0) startIndex = 0;
        if(endIndex >= data2.size()) endIndex = data2.size() - 1;

        int len = endIndex - startIndex;
        int plotAreaWidth = (m_plotWidth-rightMargin-leftMargin);
        double step = 1;

        double skipStep = (plotAreaWidth / (double)len);

        painter->beginPath();

        for (int i=startIndex, index=0; i<endIndex; i+=step, index++){

            double x1 = (index) * skipStep;
            double x2 = (index+1) * skipStep;
            double y1 = (data2.at((int)i).y() - m_min) * ratio;
            double y2 = (data2.at((int)i+1).y() - m_min) * ratio;
            y1 += bottomMargin;
            y2 += bottomMargin;

            if(x2 >= m_plotWidth-rightMargin) break;        //-- ignor out area ploting --//

            DrawLine(painter, QPoint(x1, y1), QPoint(x2, y2));
        }

        painter->stroke();

    } catch (...) {

    }

}

void AmplitudePainter::DrawGridCaption(QNanoPainter *painter, int lm, int rm, int tm, int bm, int stepSize, int dashSize, int spacing)
{

    painter->setLineCap(QNanoPainter::CAP_ROUND);
    painter->setLineJoin(QNanoPainter::JOIN_ROUND);
    painter->setLineWidth(1);

    QNanoColor lineColor("#4499ff");
    //    lineColor.setAlpha(255);
    painter->setStrokeStyle(lineColor);
    painter->setAntialias(0.0);


    painter->beginPath();

    DrawLine(painter, QPoint(m_plotWidth-rm, tm), QPoint(m_plotWidth-rm, m_plotHeight-bm-spacing)); //-- vertical line --//


    //-- horizontal dash lines --//
    for (int yPos = tm; yPos < m_plotHeight-bm-spacing; yPos+=stepSize) {
        DrawLine(painter, QPoint(m_plotWidth-rm, yPos), QPoint( m_plotWidth-rm+dashSize, yPos));
    }
    painter->stroke();


    painter->beginPath();
    lineColor = "#e0e0e0";
    painter->setStrokeStyle(lineColor);

    //-- horizontal grid lines --//
    for (int yPos = tm; yPos < m_plotHeight-bm-spacing; yPos+=stepSize*2) {
        DrawLine(painter, QPoint(lm+spacing, yPos), QPoint(m_plotWidth-rm, yPos));
    }
    //-- vertical grid lines --//
    for (int xPos = lm+spacing; xPos < m_plotWidth-rm; xPos+=stepSize*2) {
        DrawLine(painter, QPoint(xPos, m_plotHeight-bm-spacing), QPoint(xPos, tm));
    }
    painter->stroke();

    //-- captions --//
    lineColor = "#464646";
    //    painter->setTextAlign(QNanoPainter::ALIGN_CENTER);
    //    painter->setTextBaseline(QNanoPainter::BASELINE_MIDDLE);
    QNanoFont font1(QNanoFont::DEFAULT_FONT_NORMAL);
    font1.setPixelSize(10);
    painter->setFont(font1);
    painter->setFillStyle(lineColor);

    painter->save();
    painter->translate(m_plotWidth-12, m_plotHeight/2);// to correct the text's position
    painter->rotate(1.6);
    painter->fillText("dB", 0, 0);
    painter->restore();

}

void AmplitudePainter::DrawLine(QNanoPainter *painter, const QPoint &firstPoint, const QPoint &secondPoint)
{
    painter->moveTo(firstPoint);
    painter->lineTo(secondPoint);
}

QNanoColor AmplitudePainter::String2QnanoColor(const QString color)
{

    int red = color.mid(1,2).toUInt(nullptr, 16);
    int green = color.mid(3,2).toUInt(nullptr, 16);
    int blue= color.mid(5,2).toUInt(nullptr, 16);
    QNanoColor qColor;
    qColor.setRed(red);
    qColor.setGreen(green);
    qColor.setBlue(blue);
    return qColor;
}

/////////////////////////////////////////////

AmplitudeItem::AmplitudeItem(QQuickItem *parent)
    :  QNanoQuickItem(parent)
    ,m_plotWidth(500)
    ,m_plotHeight(200)
    ,m_plotStartPrc(0.0)
    ,m_plotEndPrc(1.0)
{

    data2.clear();

    //-- generate dummy data --//
    GenerateData(1000);

}


void AmplitudeItem::setPlotWidth(float pWidth)
{
    this->m_plotWidth = pWidth;
    update();
}

void AmplitudeItem::setPlotHeight(float pHeight)
{
    this->m_plotHeight = pHeight;
    update();
}

void AmplitudeItem::setPlotStartPrc(float startPrc)
{
    this->m_plotStartPrc = startPrc;
    update();
}

void AmplitudeItem::setPlotEndPrc(float endPrc)
{
    this->m_plotEndPrc = endPrc;
    update();
}

void AmplitudeItem::setPlotColor(QString color)
{
    this->m_plotColor = color;
    update();
}

void AmplitudeItem::setIsDarkTheme(bool isDarkTheme)
{
//    m_info->changeThemeColors(isDarkTheme);

    /*this->isDarkTheme = isDarkTheme;
    if(this->isDarkTheme){
        m_colorLine     = "#838383";
        m_colorText     = "#a4a4a4";
        m_colorGrid     = "#636363";
        m_colorPoint    = "#FF2366";
        m_colorPlotBlue = "#4499ff";
        m_colorPlotGrey = "#8c8c8c";
    }
    else{
        m_colorLine     = "#a0a0a0";
        m_colorText     = "#464646";
        m_colorGrid     = "#e0e0e0";
        m_colorPoint    = "#FF2366";
        m_colorPlotBlue = "#4499ff";
        m_colorPlotGrey = "#8c8c8c";
    }*/
    update();
}

void AmplitudeItem::DrawGridCaption(QNanoPainter *painter, int lm, int rm, int tm, int bm, int stepSize, int dashSize, int spacing)
{

    painter->setLineCap(QNanoPainter::CAP_ROUND);
    painter->setLineJoin(QNanoPainter::JOIN_ROUND);
    float lineWidth = QNanoPainter::mmToPx(1);
    painter->setLineWidth(lineWidth);

    QNanoColor lineColor;
    lineColor.fromQColor(QColor("#a0a0a0"));
    lineColor.setAlpha(255);
    painter->setStrokeStyle(lineColor);
    painter->setAntialias(2.0);
    //    drawPathLine(points, QPoint(0,0));

    //painter->setPen(QPen(QColor(m_info->m_colorLine), 1, Qt::SolidLine, Qt::RoundCap));

    //    painter->drawLine(m_plotWidth-rm, tm, m_plotWidth-rm, m_plotHeight-bm-spacing);    //-- vertical line --//
    DrawLine(painter, QPoint(m_plotWidth-rm, tm), QPoint(m_plotWidth-rm, m_plotHeight-bm-spacing));
    //-- horizontal dash lines --//
    for (int yPos = tm; yPos < m_plotHeight-bm-spacing; yPos+=stepSize) {
        //        painter->drawLine(m_plotWidth-rm, yPos, m_plotWidth-rm+dashSize, yPos);
        DrawLine(painter, QPoint(m_plotWidth-rm, yPos), QPoint( m_plotWidth-rm+dashSize, yPos));
    }

    //    painter->setPen(QPen(QColor(m_info->m_colorGrid), 1, Qt::SolidLine, Qt::RoundCap));
    lineColor.fromQColor(QColor("#e0e0e0"));
    painter->setStrokeStyle(lineColor);

    //-- horizontal grid lines --//
    for (int yPos = tm; yPos < m_plotHeight-bm-spacing; yPos+=stepSize*2) {
        //        painter->drawLine(lm+spacing, yPos, m_plotWidth-rm, yPos);
        DrawLine(painter, QPoint(lm+spacing, yPos), QPoint(m_plotWidth-rm, yPos));
    }
    //-- vertical grid lines --//
    for (int xPos = lm+spacing; xPos < m_plotWidth-rm; xPos+=stepSize*2) {
        //painter->drawLine(xPos, m_plotHeight-bm-spacing, xPos, tm);
        DrawLine(painter, QPoint(xPos, m_plotHeight-bm-spacing), QPoint(xPos, tm));
    }

    //-- captions --//
    //painter->setPen(QPen(QColor(m_info->m_colorText), 1, Qt::SolidLine, Qt::RoundCap));
    lineColor.fromQColor(QColor("#464646"));
    painter->setStrokeStyle(lineColor);


    painter->setTextAlign(QNanoPainter::ALIGN_CENTER);
    painter->setTextBaseline(QNanoPainter::BASELINE_MIDDLE);
    QNanoFont font1(QNanoFont::DEFAULT_FONT_BOLD_ITALIC);
    font1.setPixelSize(10);
    painter->setFont(font1);
    //painter->setFillStyle("#B0D040");
    painter->fillText("HELLO", m_plotWidth/2, m_plotHeight/2);


    /*
    QFont font = painter->font();
    font.setPixelSize(10);
    painter->setFont(font);
    QFontMetrics fm(font1);

    //-- horizontal caption --//
    const QRect rectangle2 = QRect(0
                                   , 0
                                   , fm.horizontalAdvance("dB")
                                   , fm.height());
    QRect boundingRect2;

    painter->save();
    painter->translate(m_plotWidth-2, m_plotHeight/2-fm.horizontalAdvance("dB")/2);// to correct the text's position
    painter->rotate(90);
    painter->drawText(rectangle2, 0, tr("dB"), &boundingRect2);
    painter->restore();*/
}

void AmplitudeItem::GenerateData(int sampleCoune)
{
    m_max = 0;
    m_min = 9999999;
    data2.clear();

    //qDebug() << "start generate data";
    for (int i=leftMargin; i< sampleCoune; i+=2) {

        //double y = (qSin(M_PI / 50 * i) + 0.5 + QRandomGenerator::global()->generateDouble())  * 50;
        double chance = QRandomGenerator::global()->generateDouble();
        double y = QRandomGenerator::global()->generateDouble()  * (chance > 0.8 ? 50 : chance < 0.4 ? 5 : 20);
        if(y > m_max) m_max = y;
        if(y < m_min) m_min = y;
        //        data.append(QPointF(i, y));
        data2.push_back(QPointF(i, y));

        y = -y;
        if(y > m_max) m_max = y;
        if(y < m_min) m_min = y;
        //        data.append(QPointF(i, y));
        data2.push_back(QPointF(i, y));
    }
    //qDebug() << "data generated " << m_min << ", " << m_max;

    //-- paint in init class --//
    update();
}

void AmplitudeItem::DrawLine(QNanoPainter *painter, const QPoint &firstPoint, const QPoint &secondPoint)
{

    painter->moveTo(firstPoint);
    painter->lineTo(secondPoint);
}

