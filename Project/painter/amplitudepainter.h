#ifndef AMPLITUDEPAINTER_H
#define AMPLITUDEPAINTER_H

#include "qnanoquickitem.h"
#include "qnanoquickitempainter.h"


#ifdef LOG_PAINTER
#   define Log qDebug()
#else
#   define Log nullDebug()
#endif

// HelloItemPainter contains the painting code
class AmplitudePainter : public QNanoQuickItemPainter
{
public:
    AmplitudePainter();

    // Reimplement
    void synchronize(QNanoQuickItem *item);
    void paint(QNanoPainter *p);
private:
    void DrawGridCaption(QNanoPainter *painter, int lm, int rm, int tm, int bm, int stepSize, int dashSize, int spacing);
    void DrawLine(QNanoPainter *painter, const QPoint &firstPoint, const QPoint &secondPoint);
    QNanoColor String2QnanoColor(const QString color);

    //-- property of chart --//
//    chartInfo*  m_info;


    int m_plotWidth;
    int m_plotHeight;
    float m_plotStartPrc;
    float m_plotEndPrc;
    QString m_plotColor;
    std::vector<QPointF> data2;
    int m_max;
    int m_min;
};

// HelloItem provides the API towards QML
class AmplitudeItem : public QNanoQuickItem
{
    Q_OBJECT

    Q_PROPERTY(float plotWidth WRITE setPlotWidth)
    Q_PROPERTY(float plotHeight WRITE setPlotHeight)
    Q_PROPERTY(float plotStartPrc WRITE setPlotStartPrc)
    Q_PROPERTY(float plotEndPrc WRITE setPlotEndPrc)
    Q_PROPERTY(QString plotColor WRITE setPlotColor)
    Q_PROPERTY(float isDarkTheme WRITE setIsDarkTheme)

public:
    AmplitudeItem(QQuickItem *parent = nullptr);

    // Reimplement
    QNanoQuickItemPainter *createItemPainter() const
    {
        // Create painter for this item
        return new AmplitudePainter();
    }



    void setPlotWidth(float pWidth);
    void setPlotHeight(float pHeight);
    void setPlotStartPrc(float startPrc);
    void setPlotEndPrc(float endPrc);
    void setPlotColor(QString color);
    void setIsDarkTheme(bool isDarkTheme);


    //-- global parameter --//
    int m_plotWidth;
    int m_plotHeight;
    float m_plotStartPrc;
    float m_plotEndPrc;
    QString m_plotColor;

    std::vector<QPointF> data2;
    int m_max;
    int m_min;

public slots:
    //void test();

private:

    QVector<QPointF> data;

    void DrawGridCaption(QNanoPainter *p, int lm, int rm, int tm, int bm, int stepSize, int dashSize, int spacing);
    void GenerateData(int sampleCoune);
    void inline DrawLine(QNanoPainter *p, const QPoint &firstPoint, const QPoint &secondPoint);

    int leftMargin = 2;
    int rightMargin = 25;
    int topMargin = 5;
    int bottomMargin = 5;
    int stepSize = 25;
    int dashSize = 3;
};

#endif // AMPLITUDEPAINTER_H
