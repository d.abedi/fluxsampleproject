TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    painter/amplitudepainter.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = $$PWD

# Include QNanoPainter
include(../libqnanopainter/include.pri)

# If you install QuickFlux by qpm.pri, change it to include(vendor/vendor.pri)
include(../quickflux/quickflux.pri)

DISTFILES += \
    README.md

HEADERS += \
    painter/amplitudepainter.h
