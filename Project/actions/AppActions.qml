pragma Singleton
import QtQuick 2.0
import QuickFlux 1.1
import "./"

ActionCreator {

    // Play all tracks
    signal play();

    // Stop tracks
    signal stop();

    // Change Track properties
    signal setTrack(var uid, bool filter1, bool filter2, bool visible);

    // Add new Track
    signal addTrack(string title, string file);

    // Delete Track
    signal deleteTrack(var uid);

    // Set Track Gain
    signal setGain(var uid, int gain);

    // Set Track Color
    signal setColor(var uid, string color);

    // Set filter1 properties
    signal setFilter1(real coeff1, real coeff2);

    // Open filter1 windows
    signal openFilter1Setting()

    // Set filter2 properties
    signal setFilter2(real coeff1, real coeff2);

    // Open filter2 windows
    signal openFilter2Setting()

    // Set indicator position
    signal setIndicator();

    // start application
    signal startApp()
}

