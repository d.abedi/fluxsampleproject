pragma Singleton
import QtQuick 2.0
import QuickFlux 1.0

KeyTable {
    // KeyTable is an object with properties equal to its key name

    property string play

    property string stop

    property string setTrack

    property string addTrack

    property string deleteTrack

    property string setGain

    property string setColor

    property string setFilter1

    property string openFilter1Setting

    property string setFilter2

    property string openFilter2Setting

    property string setIndicator

    property string startApp
}

